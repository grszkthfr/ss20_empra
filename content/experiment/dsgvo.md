+++
title = "DSGVO"
author = ["Jonas Großekathöfer"]
lastmod = 2020-05-15T10:56:36+02:00
draft = false
+++

**WORK IN PROGRESS**

|  | **Lehrstuhl für Psychologie I**         |
|--|-----------------------------------------|
|  |                                         |
|  | **Univ.-Prof. Dr. P. Pauli**            |
|  |                                         |
|  | Julius-Maximilians-Universität Würzburg |
|  |                                         |
|  | Marcusstraße 9-11                       |
|  |                                         |
|  | 97070 Würzburg                          |
|  |                                         |

**Verantwortliche Dienststelle**

Julius-Maximilians-Universität Würzburg

Sanderring 2<br />
D-97070 Würzburg<br />
Telefon: +49 931 31-0

E-Mail: info@uni-wuerzburg.de

**Ergänzende Information für Studienteilnehmer gemäß**

**Europäischer Datenschutz-Grundverordnung (EU-DSGVO)**[^fn:1]

**Zusätzlich werden Sie hiermit über die in der DSGVO festgelegten Rechte
informiert:**

**Rechtsgrundlage**

Rechtsgrundlage für die Verarbeitung von personenbezogenen Daten in
klinischen Studien bzw. Studien an Menschen ist Ihre Einwilligung in
schriftlicher Form gemäß DSGVO, die Deklaration von Helsinki (Erklärung
des Weltärztebundes zu den ethischen Grundsätzen für die medizinische
Forschung am Menschen) und die Leitlinie für Gute Klinische Praxis. Bei
klinischen Prüfungen mit Arzneimitteln ist zusätzlich das
Arzneimittelgesetz und bei klinischen Prüfungen mit Medizinprodukten das
Medizinproduktegesetz anzuwenden. Zeitgleich mit der DSGVO treten in
Deutschland das überarbeitete Bundesdatenschutzgesetz (BDSG-neu) und
landesdatenschutzrechtliche Regelungen in Kraft.

**Bezüglich Ihrer Daten haben Sie folgende Rechte**:

**Einwilligung zur Verarbeitung personenbezogener Daten und Recht auf
Widerruf**

Die Verarbeitung Ihrer personenbezogenen Daten ist nur mit Ihrer
Einwilligung rechtmäßig. Sie haben das Recht, Ihre Einwilligung zur
Verarbeitung personenbezogener Daten jederzeit ohne Angabe von Gründen
zu widerrufen. Es dürfen jedoch die bis zu diesem Zeitpunkt erhobenen
Daten durch die in der Probandeninformation bzw. Einwilligungserklärung
zu der jeweiligen Studie genannten Stellen verarbeitet werden. Bei
Widerruf der Einwilligung bleibt die Rechtmäßigkeit der auf ihrer
Grundlage bis zum Widerruf erfolgten Verarbeitung unberührt.

**Recht auf Auskunft, Berichtigung, Löschung usw.**

Ihnen stehen sog. Betroffenenrechte zu, d.h. Rechte, die Sie als im
Einzelfall betroffene Person ausüben können. Diese Rechte können Sie
gegenüber der Universität Würzburg geltend machen. Sie ergeben sich aus
der DSGVO.

**Recht auf Auskunft, Art. 15 DSGVO**

Sie haben das Recht auf Auskunft über die Sie betreffenden
personenbezogenen Daten, die im Rahmen der Studie erhoben, verarbeitet
oder ggf. an Dritte übermittelt wurden.

**Recht auf Berichtigung, Art. 16 DSGVO**

Wenn Sie feststellen, dass unrichtige Daten zu Ihrer Person verarbeitet
werden, können Sie eine Berichtigung verlangen. Unvollständige Daten
müssen unter Berücksichtigung des Zwecks der Verarbeitung
vervollständigt werden.

**Recht auf Löschung, Art 17 DSGVO**

Sie haben das Recht auf Löschung Ihrer personenbezogenen Daten, wenn
bestimmte Löschgründe vorliegen. Dies ist insbesondere der Fall, wenn
diese für den Zweck, zu dem sie ursprünglich erhoben oder verarbeitet
wurden, nicht mehr erforderlich sind oder wenn Sie Ihre Einwilligung
widerrufen.

**Recht auf Einschränkung der Verarbeitung, Art. 18 DSGVO**

Sie haben das Recht auf Einschränkung der Verarbeitung Ihrer Daten. Dies
bedeutet, dass Ihre Daten zwar nicht gelöscht, aber gekennzeichnet
werden, um ihre weitere Verarbeitung oder Nutzung einzuschränken.

**Recht auf Datenübertragbarkeit, Art. 20 DSGVO**

Sie haben das Recht, die Sie betreffenden personenbezogenen Daten, die
Sie uns zur Verfügung gestellt haben, in einem gängigen elektronischen
Format zu verlangen.

**Recht auf Widerspruch gegen unzumutbare Datenverarbeitung, Art. 21
DSGVO**

Sie haben grundsätzlich ein allgemeines Widerspruchsrecht auch gegen
rechtmäßige Datenverarbeitungen, die im öffentlichen Interesse liegen,
in Ausübung öffentlicher Gewalt oder aufgrund des berechtigten
Interesses einer Stelle erfolgen.

**Möchten Sie eines dieser Rechte in Anspruch nehmen, wenden Sie sich
bitte an** die unten genannte Studienleitung. Außerdem haben Sie das
**Recht, Beschwerde bei der/den Datenschutzaufsichtsbehörde/n
einzulegen,** wenn Sie der Ansicht sind, dass die Verarbeitung der Sie
betreffenden personenbezogenen Daten gegen die DSGVO verstößt.

Außerdem haben Sie das Recht auf Beschwerde bei einer Aufsichtsbehörde,
wenn Sie der Ansicht sind, dass die Verarbeitung Ihrer Daten
datenschutzrechtlich nicht zulässig ist. Die Beschwerde bei der
Aufsichtsbehörde kann formlos erfolgen. Für die
Julius-Maximilians-Universität Würzburg ist dies der Bayerische
Landesbeauftragte für den Datenschutz (BayLfD), Postfach 22 12 19, 80502
München, Telefon: 089/212672-0, Email: poststelle@datenschutzbayern.de

Die Datenverarbeitung erfolgt auf der Rechtsgrundlage Art. 6 Abs. 1
DSGVO.

Die Datenverarbeitung ist zur Wahrung der berechtigten Interessen des
Verantwortlichen oder eines Dritten erforderlich, sofern nicht die
Interessen oder Grundrechte und Grundfreiheiten der betroffenen Person
überwiegen (gem. Art. 6 Abs. 1 Buchst. f DSGVO). Ihre personenbezogenen
Daten werden nicht an Dritte, Drittländer oder internationale
Organisationen weitergegeben.

Ihre weiteren Rechte gem. der Datenschutzgrundverordnung der EU können
Sie hier einsehen:
<https://www.uni-wuerzburg.de/universitaet/datenschutzbeauftragter/>

Durch meine Unterschrift bestätige ich diese Informationen gelesen und
verstanden zu haben.

<span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline">\_\_</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span>
<span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline">\_\_</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span>

Name, Vorname Ort, Datum

<span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline">\_\_</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span>

Unterschrift

**Studienleitung**

Jonas Großekathöfer

Marcusstraße 9-11, R. 207

Telefon: +49 931 31- 84504

E-Mail: jonas.grossekathoefer@uni-wuerzburg.de

[^fn:1]: Verordnung (EU) 20\*16/679 des Europäischen Parlaments und des Rates vom 27. April 2016 zum Schutz natürlicher Personen bei der Verarbeitung personenbezogener Daten, zum freien Datenverkehr und zur Aufhebung der Richtlinie 95/46/EG (Datenschutz-Grundverordnung)\*
