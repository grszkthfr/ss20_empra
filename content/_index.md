+++
title = "Sitzungstermine"
author = ["Jonas Großekathöfer"]
lastmod = 2020-07-29T12:38:51+02:00
draft = false
+++

|    | Termin         | Thema                                                                                                    |
|----|----------------|----------------------------------------------------------------------------------------------------------|
| 1  | 20.04.2020     | [Intro](./intro.html)                                                                                    |
| 2  | 27.04.2020     | Intro II &  Was ist Wissenschaft?                                                                        |
| 3  | 04.05.2020     | Literatur & Inhalt                                                                                       |
| 4  | 11.05.2020     | ~~[Stiftungsfest](https://www.uni-wuerzburg.de/presse/jmu/publikationen/stiftungsfest)~~ Forschungsfrage |
| 5  | 18.05.2020     | Forschungsdesign I                                                                                       |
| 6  | 25.05.2020     | Forschungsdesign II (Pilotierung)                                                                        |
| -  | 01.06.2020     | [Pfingsten](https://de.wikipedia.org/wiki/Pfingsten)                                                     |
| 7  | 08.06.2020     | Erhebung                                                                                                 |
| 8  | 15.06.2020     | Erhebung/Datenverarbeitung I                                                                             |
| 9  | **29.06.2020** | Datenaufarbeitung II                                                                                     |
| 10 | 02.07.2020     | Auswertung                                                                                               |
| 11 | 06.07.2020     | Interpretation                                                                                           |
| 12 | 13.07.2020     | Poster & Video                                                                                           |
| 13 | 20.07.2020     | Poster & Video                                                                                           |
| 14 | 27.07.2020     | Prüfung                                                                                                  |
